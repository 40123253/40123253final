#@+leo-ver=5-thin
#@+node:ppython.20131221094631.1635: * @file cmslite.py
#coding: utf-8
'''
CMSlite 是一個以 Python3 與 CherryPy 編寫的羽量級內容管理系統, 主要建構流程參考 CMSimply
'''

#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:ppython.20131221094631.1636: ** <<declarations>> (cmslite)
import cherrypy
import re
import os
import sys
# 利用 textile 建立 unordered list 
import textile
import hashlib
import html

# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    sys.path.append(os.path.join(os.getenv("OPENSHIFT_REPO_DIR"), "wsgi"))
else:
    sys.path.append(_curdir)

# 設定在雲端與近端的資料儲存目錄
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 表示程式在雲端執行
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    data_dir = os.environ['OPENSHIFT_DATA_DIR']
else:
    # 表示程式在近端執行
    download_root_dir = _curdir + "/local_data/"
    data_dir = _curdir + "/local_data/"

content_filename = data_dir + "content.htm"
#@-<<declarations>>
#@+others
#@+node:ppython.20131221094631.1637: ** file_get_contents
def file_get_contents(filename):
    with open(filename, encoding="utf-8") as file:
        return file.read()
#@+node:ppython.20131221094631.1638: ** search_content
def search_content(head, page, search):
    return page[int(search)]
#@+node:ppython.20131221094631.1639: ** parse_content
def parse_content():
    if not os.path.isfile(content_filename):
        檔案 = open(content_filename, "w", encoding="utf-8")
        page_content = "<html><head></head><body>"+ \
        "<h1>heading</h1>content</body></html>"
        檔案.write(page_content)
        檔案.close()
        return ["heading"], ["content"]
    subject = file_get_contents(content_filename)
    content_sep = '#@CMSIMPLY_SPLIT@#'
    head_level = 3
    content = re.split('</body>', subject)
    result = re.sub('<h[1-'+str(head_level)+']>', content_sep, content[0])
    data = result.split(content_sep)[1:]
    head_list = []
    level_list = []
    page_list = []
    order = 1
    for 索引 in range(len(data)):
        page_data = re.sub('</h', content_sep, data[索引])
        head = page_data.split(content_sep)[0]
        order += 1
        head_list.append(head)
        page = page_data.split(content_sep)[1][2:]
        level = page_data.split(content_sep)[1][0]
        page_list.append(page)
        level_list.append(level)
    return head_list, level_list, page_list
#@+node:cmsxh.20131226100304.7228: ** parse_config
def parse_config():
    if not os.path.isfile(data_dir+"config"):
        # 假如找不到 config, 則建立新 config
        檔案 = open(data_dir+"config", "w", encoding="utf-8")
        # 內建管理者密碼為 40123253
        password="40123253"
        hashed_password = hashlib.sha512(password.encode('utf-8')).hexdigest()
        檔案.write(hashed_password)
        檔案.close()
    config = file_get_contents(data_dir+"config")
    config_data = config.split("\n")
    password = config_data[0]
    return password
#@+node:cmsxh.20131231221807.3403: ** render_menu
def render_menu(head, level, page, sitemap=0):
    目錄 = ""
    current_level = level[0]
    if sitemap:
        目錄 += "<ul>"
    else:
        目錄 += "<ul class='navigation'>"
    for 索引 in range(len(head)):
        if level[索引] > current_level:
            目錄 += "<ul>"
            目錄 += "<li><a href='get_page?heading="+str(索引)+"'>"+head[索引]+"</a>"
        elif level[索引] == current_level:
            if level[索引] == 1:
                if sitemap:
                    目錄 += "<li><a href='get_page?heading="+str(索引)+"'>"+head[索引]+"</a>"
                else:
                    目錄 += "<li class='navigation'><a href='get_page?heading="+str(索引)+"'>"+head[索引]+"</a>"
            else:
                目錄 += "<li><a href='get_page?heading="+str(索引)+"'>"+head[索引]+"</a>"
        else:
            目錄 += "</li>"*(int(current_level) - int(level[索引]))
            目錄 += "</ul>"*(int(current_level) - int(level[索引]))
            if level[索引] == 1:
                if sitemap:
                    目錄 += "<li><a href='get_page?heading="+str(索引)+"'>"+head[索引]+"</a>"
                else:
                    目錄 += "<li class='navigation'><a href='get_page?heading="+str(索引)+"'>"+head[索引]+"</a>"
            else:
                目錄 += "<li><a href='get_page?heading="+str(索引)+"'>"+head[索引]+"</a>"
        current_level = level[索引]
    目錄 += "</li></ul>"
    return 目錄
#@+node:ppython.20131221094631.1640: ** class CMSlite
class CMSlite(object):
    #@+others
    #@+node:ppython.20131221094631.1641: *3* __init__
    def __init__(self):
        if not os.path.isfile(content_filename):
            檔案 = open(content_filename, "w", encoding="utf-8")
            page_content = "<html><head></head><body>"+ \
            "<h1>heading</h1>content</body></html>"
            檔案.write(page_content)
            檔案.close()
    #@+node:ppython.20131221094631.1642: *3* default
    @cherrypy.expose
    def default(self, *args, **kwargs):
        raise cherrypy.HTTPRedirect("/")
    #@+node:ppython.20131221094631.1643: *3* index
    @cherrypy.expose
    def index(self, heading=None, *args, **kwargs):
        head, level, page = parse_content()
        目錄 = render_menu(head, level, page, 0)
        if heading == None:
            return self.layout(目錄, head, page, 0)
        else:
            return self.layout(目錄, head, page, heading)
    #@+node:cmsxh.20131231221807.3404: *3* index_old
    @cherrypy.expose
    def index_old(self, heading=None, *args, **kwargs):
        head, level, page = parse_content()
        menu = ""
        for 索引 in range(len(level)):
            menu += "*"*int(level[索引])+" <a href='get_page?heading="+str(索引)+"'>"+head[索引]+"</a>\n"
        result = textile.textile(menu).replace("\t", "")
        data = result.split("\n")
        final_menu = ""
        count = 1
        for 索引 in range(len(data)):
            if count ==1:
                final_menu += '''<ul class="navigation">'''
            else:
                final_menu += data[索引]+"\n"
            count += 1
        目錄 = final_menu
        if heading == None:
            return self.layout(目錄, head, page, 0)
        else:
            return self.layout(目錄, head, page, heading)
    #@+node:amd.20131221133050.3386: *3* edit_view
    @cherrypy.expose
    def edit_view(self, heading=None, *args, **kwargs):
        head, level, page = parse_content()
        目錄 = render_menu(head, level, page, 0)
        if heading == None:
            return self.layout(目錄, head, page, 0)
        else:
            return self.layout(目錄, head, page, heading)
    #@+node:cmsxh.20131226100304.7225: *3* edit_config
    @cherrypy.expose
    def edit_config(self):
        # 取得目錄
        head, level, page = parse_content()
        目錄 = render_menu(head, level, page, 0)
        # 取得管理密碼
        password = parse_config()
        # 編輯 config 檔案
        return self.setcss()+'''</head><body>
    <div class='wrapper'><header class='header'><a href='/'>首頁</a>|<a href='edit_page'>全頁編輯</a><br /><br />'''+ \
    目錄+"</header><h1>編輯設定檔</h1><form method='post' action='saveConfig'> \
    Password:<input type='text' name='password' value='"+password+"' size='50'><br /> \
    <input type='hidden' name='password2' value='"+password+"'> \
    <input type='submit' value='send'></form> \
    </main></div></body></html>"
    #@+node:cmsxh.20131226100304.7226: *3* saveConfig
    @cherrypy.expose
    def saveConfig(self, password=None, password2=None):
        if password == None or password2 == None:
            return "<a href='/'>首頁</a>|<a href='edit_page'>編輯</a><br /><br />錯誤!請連絡管理員!"
        # 查驗舊密碼
        old_password = parse_config()
        # 取得目錄
        head, level, page = parse_content()
        目錄 = render_menu(head, level, page, 0)
        if password == None or password2 != old_password or password == '':
            return self.setcss()+"</head><body><div class='wrapper'><header class='header'><a href='/'>首頁</a>|<a href='edit_page'>全頁編輯</a><br /><br />"+ \
            目錄+"</header><main><h1>錯誤!</h1></main></div></body></html>"
        else:
            if password == password2 and password == old_password:
                hashed_password = old_password
            else:
                hashed_password = hashlib.sha512(password.encode('utf-8')).hexdigest()
            檔案 = open(data_dir+"config", "w", encoding="utf-8")
            檔案.write(hashed_password)
            檔案.close()
            return self.setcss()+"</head><body><div class='wrapper'><header class='header'><a href='/'>首頁</a>|<a href='edit_page'>全頁編輯</a><br /><br />"+ \
            目錄+"</header><main><h1>設定已經存檔</h1></main></div></body></html>"

    #@+node:ppython.20131221094631.1644: *3* get_page
    @cherrypy.expose
    def get_page(self, heading=None, edit=0, *args, **kwargs):
        if edit ==0:
            try:
                head, level, page = parse_content()
                目錄 = render_menu(head, level, page, 0)
                if heading == None:
                    raise cherrypy.HTTPRedirect("/")
                else:
                    return self.layout(目錄, head, page, heading)
            except:
                raise cherrypy.HTTPRedirect("/")
        else:
            try:
                head, level, page = parse_content()
                頁面內容 = html.escape("<h"+level[int(heading)]+">"+head[int(heading)]+"</h"+level[int(heading)]+">"+page[int(heading)])
                head, level, page = parse_content()
                目錄 = render_menu(head, level, page, 0)
                if heading == None:
                    raise cherrypy.HTTPRedirect("/")
                else:
                    # 準備 image_link 與 link_list
                    files = os.listdir(download_root_dir+"downloads/")
                    link_list = "["
                    image_list = "["
                    for 索引 in range(len(files)):
                        file_url = "/download/?filepath="+download_root_dir.replace('\\', '/')+"downloads/"+files[索引]
                        link_list += "{title:'"+files[索引]+"', value:'"+file_url+"'}"
                        if 索引 != len(files)-1:
                            link_list += ","
                    link_list += "]"
                    # 處理影像檔案連結
                    images = os.listdir(download_root_dir+"downloads/")
                    for 索引 in range(len(images)):
                        if images[索引].endswith(".png") or images[索引].endswith(".jpg") or images[索引].endswith(".gif"):
                            image_url = "/downloads/"+images[索引]
                            image_list += "{title:'"+images[索引]+"', value:'"+image_url+"'}"
                            if 索引 != len(images)-1:
                                image_list += ","
                    image_list += "]"
                    outstring = self.setcss()+'''</head><body><script type="text/javascript" src="/static/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    image_list:'''+image_list+''',
    link_list:'''+link_list+''',
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code"
    });
    </script>
    '''
                    outstring += "<div class='wrapper'><header class='header'><a href='/'>首頁</a>|<a href='edit_page'>全頁編輯</a><br /><br />"+ \
                        目錄+"</header><main class='content'><form method='post' action='ssavePage'><div style='z-index: -1;'> \
    <textarea name='page_content' cols='50' rows='10'>"+頁面內容+"</textarea></div> \
    <input type='hidden' name='page_order' value='"+str(heading)+"'> \
    <input type='submit' value='send'></form></main></div></body></html>"
                    return outstring
            except:
                raise cherrypy.HTTPRedirect("/")
            
    #@+node:ppython.20131221094631.1645: *3* edit_page
    @cherrypy.expose
    def edit_page(self):
        head, level, page = parse_content()
        目錄 = render_menu(head, level, page, 0)
        網站內容 = html.escape(file_get_contents(content_filename))
        # 準備 image_link 與 link_list
        files = os.listdir(download_root_dir+"downloads/")
        link_list = "["
        image_list = "["
        for 索引 in range(len(files)):
            file_url = "/download/?filepath="+download_root_dir.replace('\\', '/')+"downloads/"+files[索引]
            link_list += "{title:'"+files[索引]+"', value:'"+file_url+"'}"
            if 索引 != len(files)-1:
                link_list += ","
        link_list += "]"
        # 處理影像檔案連結
        images = os.listdir(download_root_dir+"downloads/")
        for 索引 in range(len(images)):
            if images[索引].endswith(".png") or images[索引].endswith(".jpg") or images[索引].endswith(".gif"):
                image_url = "/downloads/"+images[索引]
                image_list += "{title:'"+images[索引]+"', value:'"+image_url+"'}"
                if 索引 != len(images)-1:
                    image_list += ","
        image_list += "]"
        outstring = self.setcss()+'''</head><body><script type="text/javascript" src="/static/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ], 
    image_list:'''+image_list+''',
    link_list:'''+link_list+''',
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
    </script>
    '''
        outstring += "<div class='wrapper'><header class='header'><a href='/'>首頁</a><br /><br />"+ \
            目錄+"</header><main class='content'><form method='post' action='savePage'><div style='z-index: -1;'> \
     <textarea name='page_content' cols='50' rows='10'>"+網站內容+"</textarea></div> \
     <input type='submit' value='send'></form></main></div></body></html>"
        return outstring
        
    #@+node:ppython.20131221094631.1646: *3* savePage
    @cherrypy.expose
    def savePage(self, page_content=None):
        if page_content == None:
            return "<a href='/'>首頁</a>|<a href='edit_page'>編輯</a><br /><br />錯誤!請連絡管理員!"
        檔案 = open(content_filename, "w", encoding="utf-8")
        page_content = page_content.replace("\n","")
        檔案.write(page_content)
        檔案.close()
        return "<a href='/'>首頁</a>|<a href='edit_page'>編輯</a><br /><br />已經存檔!"
    #@+node:amd.20131221133050.3385: *3* ssavePage
    @cherrypy.expose
    def ssavePage(self, page_content=None, page_order=None):
        if page_content == None or page_order == None:
            return "<a href='/'>首頁</a>|<a href='edit_page'>編輯</a><br /><br />錯誤!請連絡管理員!"
        page_content = page_content.replace("\n","")
        head, level, page = parse_content()
        檔案 = open(data_dir+"/content.htm", "w", encoding="utf-8")
        for 索引 in range(len(head)):
            if 索引 == int(page_order):
                檔案.write(page_content)
            else:
                檔案.write("<h"+str(level[索引])+">"+str(head[索引])+"</h"+str(level[索引])+">"+str(page[索引]))
        檔案.close()
        return "<a href='/'>首頁</a>|<a href='edit_page'>全頁編輯</a>|<a href='edit_view'>單頁編輯</a><br /><br />頁面已經存檔!"
    #@+node:ppython.20131221094631.1647: *3* setcss
    def setcss(self):
        if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
            ssl = '''
    <script type="text/javascript">
    if ((location.href.search(/http:/) != -1) && (location.href.search(/login/) != -1)) \
    window.location= 'https://' + location.host + location.pathname + location.search;
    </script>
    '''
        else:
            ssl = ""

        outstring = '''
    <!doctype html><html><head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <title>CMSimply - 羽量級內容管理系統</title><link rel="stylesheet" type="text/css" href="/static/cmsimply.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <!-- for source code highlight -->
    <link rel="stylesheet" href="/static/highlight_styles/railscasts.css">
    <script src="/static/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <!-- ends source code highlight -->
    <script type="text/javascript">
            $(function(){
                $("ul.navigation > li:has(ul) > a").append('<div class="arrow-bottom"></div>');
                $("ul.navigation > li ul li:has(ul) > a").append('<div class="arrow-right"></div>');
            });
    </script>
    '''+ssl+'''
    <style type="text/css">

        /*
            作者：Yuxin
            教學文：http://fundesigner.net/only-css-menu
            授權：MIT License
        */

        /* 初始化 */
        body, ul, li, a{
            margin: 0;
            padding: 0;
            font-size: 13px;
            text-decoration: none;
        }
        ul, li {
            list-style: none;
            z-index:999;
        }
        /* 選單 li 之樣式 */
        ul.navigation li {
            position: relative;
            float: left;
        }
        /* 選單 li 裡面連結之樣式 */
        ul.navigation li a{
            display: block;
            padding: 12px 20px;
            background: #888;
            color: #EFF;
        }
        /* 特定在第一層，以左邊灰線分隔 */
        ul.navigation > li > a{
            border-bottom: 1px solid #CCC;				
            border-left: 1px solid #CCC;
            
        }
        ul.navigation > li > a:hover{
            color: #666;
            background: #DDD
        }
        /* 特定在第一層 > 第二層或以後下拉部分之樣式 */
        ul.navigation ul{
            display: none;
            float: left;
            position: absolute;			
            left: 0;	
            margin: 0;
        }
        /* 當第一層選單被觸發時，指定第二層顯示 */
        ul.navigation li:hover > ul{
            display: block;
        }			
        /* 特定在第二層或以後下拉部分 li 之樣式 */
        ul.navigation ul li {
            border-bottom: 1px solid #DDD;
        }
        /* 特定在第二層或以後下拉部分 li （最後一項不要底線）之樣式 */
        ul.navigation ul li:last-child {
            border-bottom: none;
        }
        /* 第二層或以後選單 li 之樣式 */
        ul.navigation ul a {
            width: 120px;
            padding: 10px 12px;	
            color: #666;		
            background: #EEE;			
        }
        ul.navigation ul a:hover {		
            background: #CCC;				
        }
        /* 第三層之後，上一層的選單觸發則顯示出來（皆為橫向拓展） */
        ul.navigation ul li:hover > ul{
            display: block;
            position: absolute;
            top: 0;				
            left: 100%;
        }
        /* 箭頭向下 */
        .arrow-bottom {
            display: inline-block;
            margin-left: 5px;
            border-top: 4px solid #FFF;
            border-right: 4px solid transparent;				
            border-left: 4px solid transparent;		
            width: 1px;
            height: 1px;
        }

        /* 箭頭向右 */
        .arrow-right {
            display: inline-block;
            margin-left: 12px;	
            border-top: 3px solid transparent;
            border-bottom: 3px solid transparent;
            border-left: 3px solid #666;		
            width: 1px;
            height: 1px;
        }
    </style>		
    '''
        return outstring
    #@+node:ppython.20131221094631.1648: *3* layout
    def layout(self, 目錄=None, head=None, page=None, heading=None):
        if not cherrypy.request.query_string:
            return self.setcss()+"</head><body><div class='wrapper'><header class='header'><a href='/'>首頁</a>|<a href='edit_page'>全頁編輯</a><br /><br />"+ \
        目錄+"</header><h1>"+head[int(heading)]+"</h1><main class='content'>"+search_content(head, page, heading)+"</main></div></body></html>"
        else:
            return self.setcss()+"</head><body><div class='wrapper'><header class='header'><a href='/'>首頁</a>|<a href='edit_page'>全頁編輯</a>|<a href='"+cherrypy.request.base + cherrypy.request.path_info+"?"+cherrypy.request.query_string+"&edit=1'>編輯</a><br /><br />"+ \
        目錄+"</header><h1>"+head[int(heading)]+"</h1><main class='content'>"+search_content(head, page, heading)+"</main></div></body></html>"
    #@-others
#@-others
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))

application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"}
    }

if __name__ == '__main__':
    cherrypy.quickstart(CMSlite(), config = application_conf)
#@-leo
